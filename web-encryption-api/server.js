const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

app.post('/encode', (req, res) => {
    const encoded = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encoded});
});

app.post('/decode', (req, res) => {
    const decoded = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({decoded});
});

app.listen(port, () => {
    console.log(port);
});
