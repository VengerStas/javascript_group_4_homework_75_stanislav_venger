import React, { Component } from 'react';
import './App.css';
import Forms from "./components/Forms/Forms";

class App extends Component {
  render() {
    return (
      <div className="App">
          <Forms/>
      </div>
    );
  }
}

export default App;
