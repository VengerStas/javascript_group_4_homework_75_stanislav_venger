import React, {Component} from 'react';

import './Forms.css';
import {connect} from "react-redux";
import {changeValue, decodeTextPost, encodeTextPost} from "../../store/actions";

class Forms extends Component {
    encodeHandler = (e) => {
        e.preventDefault();
        const data = {
            message: this.props.state.encode,
            password: this.props.state.password
        };
        this.props.encodeTextPost(data);
    };

    decodeHandler = (e) => {
        e.preventDefault();
        const data = {
            message: this.props.state.decode,
            password: this.props.state.password
        };
        this.props.decodeTextPost(data);
    };

    render() {
        return (
            <div>
                <form className="form">
                    <label>Decode message:</label>
                    <textarea value={this.props.state.encode} name={'encode'} onChange={this.props.changeValue} cols="30" rows="10" />
                    <label>Password</label>
                    <input name={'password'} value={this.props.state.password} onChange={this.props.changeValue} type="text"/>
                    <div className="controls">
                        <button type="submit" onClick={(e) => this.encodeHandler(e)}>Encode</button>
                        <button type="submit" onClick={(e) => this.decodeHandler(e)}>Decode</button>
                    </div>
                    <label>Encode message:</label>
                    <textarea name={'decode'} value={this.props.state.decode} onChange={this.props.changeValue} id="" cols="30" rows="10" />
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    state: state
});

const mapDispatchToProps = dispatch => ({
    changeValue: (event) => dispatch(changeValue(event)),
    decodeTextPost: (data) => dispatch(decodeTextPost(data)),
    encodeTextPost: (data) => dispatch(encodeTextPost(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Forms);
