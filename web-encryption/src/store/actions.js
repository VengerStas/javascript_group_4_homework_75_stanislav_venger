import axios from '../axios-Base';

export const ENCODE_SUCCESS = 'ENCODE_SUCCESS';
export const DECODE_SUCCESS = 'DECODE_SUCCESS';
export const CHANGE_VALUE = 'CHANGE_VALUE';


export const changeValue = (event) => ({type: CHANGE_VALUE, event});
export const encodeSuccess = (code) => ({type: ENCODE_SUCCESS, code});
export const decodeSuccess = (code) => ({type: DECODE_SUCCESS, code});


export const encodeTextPost = (data) => {
    return dispatch => {
        return axios.post('/encode', data).then(
            (response) => dispatch(encodeSuccess(response.data.encoded))
        )
    }
};

export const decodeTextPost = (data) => {
    return dispatch => {
        return axios.post('/decode', data).then(
            (response) => dispatch(decodeSuccess(response.data.decoded))
        )
    }
};
