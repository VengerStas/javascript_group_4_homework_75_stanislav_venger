import {CHANGE_VALUE, DECODE_SUCCESS, ENCODE_SUCCESS} from "./actions";

const initialState = {
    decode: '',
    password: '',
    encode: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_VALUE:
            return {
                ...state,
                [action.event.target.name]: action.event.target.value
            };
        case ENCODE_SUCCESS:
            return {...state, decode: action.code};
        case DECODE_SUCCESS:
            return {...state, encode: action.code};
        default:
            return state;
    }
};


export default reducer;
